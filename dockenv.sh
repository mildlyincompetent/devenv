#!/usr/bin/env bash

alias dockenv='docker run -it -v /run/user/$UID/gnupg/S.gpg-agent:/gpg-agent/gpg -v /run/user/$UID/gnupg/S.gpg-agent.ssh:/gpg-agent/ssh -v ~/.stack:/stack -v $PWD:/code -e TERM'
