# DevEnv

This repo contains a collection of docker images and helpful scripts which I use for development.

I use this in order to be able to run whatever OS I feel like running, but still be able to develop in consistent envrionments.

Note that this repo is under heavy development, so the documentation included here may not be perfectly accurate.

External contributions are welcome.

## How Use?

1. Acquire Docker.

2. Fork repo.

3. Modify bits where it says I'm me so that it says you're you instead. In particular, you may wish to modify the dotfiles found under `context`. Also the bit where git is configured in dockerfiles under `base`.

4. Modify `dockenv.sh` so that the command mounts things that actually exist on your system. Specifcally, the GPG agent and the GPG SSH agent need to be pointed at wherever those live on your system for GPG stuff to work. Also worth noting is the stack directory used for haskell. This needs to be owned by root (though you can chown in a dockenv) or stack will complain.

5. `build.sh`

6. `dockenv devbase/<image>` to enter an environment defined in `base`.

7. `dockenv dev/<image>` to enter an environment defined in `custom`.

8. Your working directory when you enter the environment is mounted on `/code`.

## What contributions be useful?

Parametrizing it a little so `build.sh` takes things like git attributes from the host system as opposed to being explicitly defined in dockerfiles.

Actually, creating the dockerfiles using `make` and dodgy shell scripts would be pretty neat now that I think about it. 

Contributing additional custom envrionments or improving the base envrionments.

Generally making it so that the "personalised" aspects are nicely separated from the generic aspects so that the personalised aspects can be gitignored away.

## Known issues

- If you're using pinentry-curses it gets *very* angry at docker's pseudo-tty.
- Lack of IPv6 support.
