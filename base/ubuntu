FROM ubuntu:19.04
MAINTAINER contact@kajetan.ch
RUN apt-get update
RUN yes | unminimize
RUN apt-get install -y \
  vim \
  zsh \
  git \
  tmux \
  gpg \
  curl \
  iputils-ping \
  pcscd \
  scdaemon \
  usbutils \
  locales \
  make \
  tar \
  cloc \
  psmisc \
  build-essential \
  software-properties-common
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
COPY vim /root/.vimrc
ENV EDITOR vim
RUN git config --global user.email "contact@kajetan.ch" &&\
  git config --global user.name "Kajetan Champlewski" &&\
  git config --global commit.gpgSign true
COPY ownertrust ownertrust
COPY pubkeys pubkeys
RUN gpg --import pubkeys && rm pubkeys &&\
  gpg --import-ownertrust ownertrust && rm ownertrust
COPY gpg /root/.gnupg/gpg.conf
COPY gpg-agent /root/.gnupg/gpg-agent.conf
ENV SSH_AUTH_SOCK /gpg-agent/ssh
ENV GPG_AGENT_INFO /gpg-agent/gpg
ENV GPG_AGENT_SOCK /gpg-agent/gpg
COPY gpg-socket /root/.gnupg/S.gpg-agent
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" &&\
  cd ~/.oh-my-zsh/custom/plugins &&\
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git &&\
  git clone https://github.com/zsh-users/zsh-autosuggestions.git
COPY zsh /root/.zshrc
ENV ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE fg=4
CMD zsh

